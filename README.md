## To Run this code
1. Follow backend readme to setup backend service first.
2. Deploy file under dist to web server for product build.
3. Source code are under src/app.
4. To setup dev environment, please follow guide from [Angular](https://angular.io/guide/quickstart)
5. Unit test codes are in *.spec.ts file.