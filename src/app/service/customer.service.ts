import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule, HttpResponse } from '@angular/common/http';

import { Api } from '../common/api';
import { CustomerEntity } from '../entity/customer.entity';


@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  customers: CustomerEntity[];

  constructor(private http: HttpClient) {
    this.customers = [];
  }

  listCustomers() {
    const promise = new Promise((resolve, reject) => {
      this.http.get(Api.GET_CUSTOMERS)
        .toPromise()
        .then(
          res => {
            this.customers = res as CustomerEntity[];
            resolve();
          },
          msg => {
            reject();
          }
        );
    });

    return promise;
  }
}
