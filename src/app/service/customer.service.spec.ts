import { TestBed, inject } from '@angular/core/testing';

import { CustomerService } from './customer.service';

describe('CustomerService', () => {
  let service: CustomerService;
  beforeEach(() => {
    service = new CustomerService();
  });
  it('should have property customers', () => {
    expect(service.customers).toBeDefined();
  });
});
