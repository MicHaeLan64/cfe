import { Component } from '@angular/core';
import { CustomerService } from './service/customer.service';
import { CustomerEntity } from './entity/customer.entity';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  public customers: CustomerEntity[];

  constructor(private customerService: CustomerService) { }

  getCustomers() {
    this.customerService.listCustomers().then(() => {
      this.customers = this.customerService.customers;
    });
  }
}
