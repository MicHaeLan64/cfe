export class CustomerEntity {
  constructor(
    public id: number,
    public firstName: string,
    public lastName: string,
    public email: string,
    public gender: string,
    public ipAddress: string,
    public company: string,
    public city: string,
    public title: string,
    public website: string
  ) {}
}
